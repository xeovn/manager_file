import os
from flask import Flask, request, redirect, url_for,render_template,send_from_directory,flash, session
from werkzeug import secure_filename
import sqlite3
from datetime import datetime
from database import dbmanager
from libEncrypts import crypt


UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)),"upload")
ALLOWED_EXTENSIONS = set(['txt','exe'])
DB_FILE = "data.db"
TEMPLATE_FOLDER = os.path.dirname(os.path.realpath(__file__))
app = Flask(__name__,template_folder= TEMPLATE_FOLDER)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = crypt.generate_strings()

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/')
def index():
	return render_template('login.html')

@app.route('/upload/<sessionid>')
def upload(sessionid):
	return render_template('upload.html',sessionid = sessionid)

@app.route('/login', methods=['POST'])    
def login():
	conn = dbmanager.connectionDB(DB_FILE)

	username_input = request.form['username']
	password_input = request.form['password']

	query = '''SELECT username,password FROM user WHERE username = ?'''
	data = dbmanager.selectDB(conn,query,username_input)
	if data == []:
		pass
	else:
		if crypt.verify_hash(password_input, data[0][1]):
			session['username'] = data[0][0]
			return render_template('upload.html', sessionid = session['username'])
	return render_template('login.html')



@app.route("/upload_file/<username_upload>", methods=['GET', 'POST'])
def upload_file(username_upload):
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            print filename
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            conn = dbmanager.connectionDB(DB_FILE)
            sql = '''insert into file_upload(username,url,time_upload) values (?,?,?)'''
            dbmanager.insertDB(conn,sql,username_upload,filename,datetime.now())
            dbmanager.closeDB(conn)
            return redirect(url_for('upload',sessionid = username_upload))

@app.route('/show')
def show():
    conn = dbmanager.connectionDB(DB_FILE)
    cur = conn.cursor()
    cur.execute("SELECT * FROM file_upload")
    data =cur.fetchall()
    dbmanager.closeDB(conn)
    return render_template('download.html', data = data)


@app.route('/download_file/<filename>')
def download_file(filename):
    """Download a file."""
    return send_from_directory(UPLOAD_FOLDER, filename, as_attachment=True)

if __name__ == '__main__':
	app.run(debug = True,threaded=True)