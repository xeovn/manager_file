import sqlite3

def connectionDB(db_file):
    try:
        conn = sqlite3.connect(db_file,check_same_thread=False)
        return conn
    except Error as e:
        print(e)
 
    return None

def insertDB(conn,query,username,url,datetime):
	try:
		cur = conn.cursor()
		cur.execute(query,(username,url,datetime))
		conn.commit()
	except Exception as e:
		raise e

def selectDB(conn,query,value):
	try:
		cur = conn.cursor()
		cur.execute(query,(value,))
		data = cur.fetchall()
	except Exception as e:
		raise e
	return data

def closeDB(conn):
	conn.close()