try:
	from passlib.hash import sha256_crypt
except Exception as e:
	raise e

import random
import string

def generate_strings():
	char_set = string.ascii_uppercase + string.digits
	return ''.join(random.sample(char_set*32, 32))
def generate_hash(password):
	return sha256_crypt.encrypt(password)

def verify_hash(plaintext,password_hash):
	return (sha256_crypt.verify(plaintext,password_hash))
